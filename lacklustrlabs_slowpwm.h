#pragma once

#include <stdint.h>
#if defined(ARDUINO) && ARDUINO >=100
  #include <Arduino.h>
#else
  #include <WProgram.h>
#endif


namespace lacklustrlabs {

typedef enum {
  StuckLow = 1,
  StuckHigh = 2,
  Pulsing = 3
} SlowPWMBaseMode;

class SlowPWMBase {
  public:
    SlowPWMBase(uint32_t onPeriod, uint32_t offPeriod):
      _onPeriod(onPeriod), _offPeriod(offPeriod), _mode(Pulsing) {
      begin();
    }

    void begin() {
      _state = true;
      _nextEvent = millis() + _onPeriod;
    }

    void begin(uint32_t onPeriod, uint32_t offPeriod) {
      _onPeriod = onPeriod;
      _offPeriod = offPeriod;
      begin();
    }

    inline bool getState() {
      return _state;
    }

    inline SlowPWMBaseMode getMode() {
      return (SlowPWMBaseMode)_mode;
    }

    void setMode(SlowPWMBaseMode mode) {
      _mode = mode;
      if (_mode == Pulsing) {
        if (_state){
          _nextEvent = _onPeriod + millis();
        } else {
          _nextEvent = _offPeriod + millis();
        }
      } else {

      }
    }

    inline bool isDue() {
      uint32_t now = millis();
      bool rv = false;

      if (_mode==Pulsing){
        if (_state){
          if (_nextEvent <= now ) {
            _nextEvent += _offPeriod;
            rv = true;
            while (false && _nextEvent <= now ) {
              Serial.println(F("Overflow on"));
              // we have missed an entire period, - restart
              _nextEvent += _onPeriod + _offPeriod;
            }
            _state = false;
          }
        } else {
          if (_nextEvent <= now ) {
            _nextEvent += _onPeriod;
            rv = true;
            while (false && _nextEvent <= now ) {
              Serial.println(F("Overflow off"));
              // we have missed an entire period, - restart
              _nextEvent += _onPeriod + _offPeriod;
            }
            _state = true;
          }
        }
      } else if (_mode==StuckLow ||_mode==StuckHigh){
        if (_mode==StuckLow && _state) {
          rv = true;
        } else if(_mode==StuckHigh && !_state){
          rv = true;
        } else {
          rv = false;
        }
      }
      return rv;
    }

    uint32_t _nextEvent;
    uint32_t _onPeriod;
    uint32_t _offPeriod;
    bool _state;
    uint8_t _mode;
};

}
